// lets practice creating components using JSX syntax
// the purpose of this component is to be the hero section of our page
import { Jumbotron, Container, Button } from 'react-bootstrap';// i;; be using a named export method to acquire my components from react-bootstrap
// lets acquire the bootstrap grid system
import { Row, Col } from 'react-bootstrap';

// lets acquire a link component directly from next JS
import Link from 'next/link'

// i will create a function that will return the sturcture of my component
// for this example, lets create this function in an ES6 format

const Banner = ({data}) => {
	// lets declare a return scope to determine the anatomy of the element.

	// the reason behind the behavior of the component is that React has a feature that removes whitespaces at the beginning of a line

	// and also react has 

	// lets destructure the data prop into its properties
	const { title, content, destination, label } = data
	return (
		<Container className="mt-5 pt-5">
			<Row className="mt-5 pt-5">
				<Col className="mt-4 pt-4">
					<Jumbotron>
						<h1> {title} </h1>
						<p> {content} </p>
						<Link href={destination}>
							<a> {label} </a>
						</Link>			
					</Jumbotron>
				</Col>		
			</Row>	
		</Container>
		)
}

export default Banner;