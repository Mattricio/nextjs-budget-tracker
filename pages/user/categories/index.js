import { Table, Button } from 'react-bootstrap';
import Link from 'next/link';
import View from '../../../components/View';
import AppHelper from '../../../app-helper.js';
import { useState, useEffect } from 'react';

import dynamic from 'next/dynamic';
const ParticlesBg = dynamic(() => import("particles-bg"), { ssr: false });



const categoryIndex = (props) => {
    const [data, setData] = useState(null)
        useEffect( async () => {
            if (AppHelper.getAccessToken() == undefined) {
                return;
                }
    const payload = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${AppHelper.getAccessToken()}`
        }
    }
        console.log("https://infinite-crag-62320.herokuapp.com/api")
            const res = await fetch("https://infinite-crag-62320.herokuapp.com/api/users/get-categories", payload)
            const categories = await res.json()
                console.log(categories)
                setData(categories)
            }, [])

    return (
        <View title="TRAK | Categories">
        <ParticlesBg type="cobweb" bg={true} />
            <h3>Categories</h3>
            <Link href="/user/categories/new">
                <a className="btn btn-success mt-1 mb-3">Add</a>
            </Link>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Category</th>
                        <th>Type</th>
                    </tr>
                </thead>
                <tbody>
                  {data ? data.map(category => (
                    <CategoriesView data={category}/ >
                    ))
                    :
                    <></>}
                </tbody>
            </Table>
        </View>
    )
}
export default categoryIndex;

const CategoriesView = ({data}) => {
    console.log(data)

    return (
        <>
            <tr>
                <td>
                    {data.name}
                </td>
                <td>
                    {data.type}
                </td>
            </tr>
        </>

    )
}
