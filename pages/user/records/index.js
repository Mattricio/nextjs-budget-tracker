import { Card, Button, Row, Col, InputGroup, FormControl, Form, Table } from 'react-bootstrap';
import Link from 'next/link';
import View from '../../../components/View';
import AppHelper from '../../../app-helper.js';
import { useState, useEffect } from 'react';

import dynamic from 'next/dynamic'
const ParticlesBg = dynamic(() => import("particles-bg"), { ssr: false })


const recordIndex = (props) => {

    const [data, setData] = useState(null)
        useEffect( async () => {
            if (AppHelper.getAccessToken() == undefined) {
                return;
                }
    const payload = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${AppHelper.getAccessToken()}`
        }
    }
        console.log("https://infinite-crag-62320.herokuapp.com/api")
            const res = await fetch("https://infinite-crag-62320.herokuapp.com/api/users/get-records", payload)
            const records = await res.json()
                console.log(records)
                setData(records)
            }, [])



    return (
        <View title="TRAK | Records">
        <ParticlesBg type="cobweb" bg={true} />
            <h3>Records</h3>
            <InputGroup className="mb-2">
                <InputGroup.Prepend>
                    <Link href="/user/records/new">
                        <a className="btn btn-success">Add</a>
                    </Link>
                </InputGroup.Prepend>
                <FormControl placeholder="Search Record" />
                <Form.Control as="select">
                    <option value="All">All</option>
                    <option value="Income">Income</option>
                    <option value="Expense">Expense</option>
                </Form.Control>
            </InputGroup>
            {data ? data.map(record => (
                <RecordsView data={record} />))
            :
            <></>}
        </View>
    )
}

export default recordIndex;

const RecordsView = ({data}) => {   
    console.log(data)
    return (
        <>
                        <Card className="mb-3">
                            <Card.Body>
                                <Row>
                                    <Col xs={ 6 }>
                                        <h5>{data.description}</h5>
                                        <h6>
                                          <span>{data.type}({data.categoryName})</span> 
                                        </h6>
                                        <p>{data.dateAdded}</p>
                                    </Col>
                                    <Col xs={ 6 } className="text-right">
                                        <h6>{data.amount}</h6>
                                        <span>{data.balanceAfterTransaction}</span>
                                    </Col>
                                </Row>
                            </Card.Body>
                        </Card>
                    
        </>
    )
}

