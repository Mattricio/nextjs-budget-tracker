import Head from 'next/head'
import Navbar from '../components/NavBar'
import Banner from '../components/Banner'
import View from '../components/View'
import { Container, Row, Col } from 'react-bootstrap';
//lets integrate the particles-bg dependency.
import dynamic from 'next/dynamic'
const ParticlesBg = dynamic(() => import("particles-bg"), { ssr: false })


export default function Home() {
  //lets create the "data" object which represents the props being passed down in our banner component
  const data = {
    title: "Welcome to Trak",
    content: "The leading Budget Tracker app in the market",
    destination: "/login",
    label: "Try it Now!"
  }

  return (
    <View title={'TRAK | Landing Page'}>
      <ParticlesBg type="cobweb" bg={true} />
      <Row className="justify-content-center">
        <Col xs md="6">
          <Banner data={ data } />
        </Col>
      </Row>
    </View> 
  )
}
