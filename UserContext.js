import { createContext } from 'react'// our new task is to create a context data for our app which will describe the state of our user

// 1. lets use the constructr=or to create a context object
const UserContext=createContext();

export default UserContext;

export const UserProvider = UserContext.Provider;